/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace System.Collections.Specialized
{
    using Codefarts.Localization;

    public class NotifyCollectionChangedEventArgs : EventArgs
    {
        private NotifyCollectionChangedAction action;

        private IList newItems;

        private int newStartingIndex;

        private IList oldItems;

        private int oldStartingIndex;

        public NotifyCollectionChangedAction Action
        {
            get
            {
                return this.action;
            }
        }

        public IList NewItems
        {
            get
            {
                return this.newItems;
            }
        }

        public int NewStartingIndex
        {
            get
            {
                return this.newStartingIndex;
            }
        }

        public IList OldItems
        {
            get
            {
                return this.oldItems;
            }
        }

        public int OldStartingIndex
        {
            get
            {
                return this.oldStartingIndex;
            }
        }

        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action)
        {
            this.newStartingIndex = -1;
            this.oldStartingIndex = -1;
            if (action == NotifyCollectionChangedAction.Reset)
            {
                this.InitializeAdd(action, null, -1);
                return;
            }
            
            object[] objArray = new object[1];
            objArray[0] = 4;
            throw new ArgumentException(string.Format(string.Format(LocalizationManager.Instance.Get("ERR_WrongActionForCtor"), objArray), "action"));
        }

        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, object changedItem)
        {
            this.newStartingIndex = -1;
            this.oldStartingIndex = -1;
            if (action == NotifyCollectionChangedAction.Add || action == NotifyCollectionChangedAction.Remove || action == NotifyCollectionChangedAction.Reset)
            {
                if (action != NotifyCollectionChangedAction.Reset)
                {
                    object[] objArray = new object[1];
                    objArray[0] = changedItem;
                    this.InitializeAddOrRemove(action, objArray, -1);
                    return;
                }
                
                if (changedItem == null)
                {
                    this.InitializeAdd(action, null, -1);
                    return;
                }
                    
                throw new ArgumentException(LocalizationManager.Instance.Get("ERR_ResetActionRequiresNullItem"), "action");
            }
            
            throw new ArgumentException(LocalizationManager.Instance.Get("ERR_MustBeResetAddOrRemoveActionForCtor"), "action");
        }

        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, object changedItem, int index)
        {
            this.newStartingIndex = -1;
            this.oldStartingIndex = -1;
            if (action == NotifyCollectionChangedAction.Add || action == NotifyCollectionChangedAction.Remove || action == NotifyCollectionChangedAction.Reset)
            {
                if (action != NotifyCollectionChangedAction.Reset)
                {
                    object[] objArray = new object[1];
                    objArray[0] = changedItem;
                    this.InitializeAddOrRemove(action, objArray, index);
                    return;
                }
                
                if (changedItem == null)
                {
                    if (index == -1)
                    {
                        this.InitializeAdd(action, null, -1);
                        return;
                    }
                        
                    throw new ArgumentException(LocalizationManager.Instance.Get("ERR_ResetActionRequiresIndexMinus1"), "action");
                }
                    
                throw new ArgumentException(LocalizationManager.Instance.Get("ERR_ResetActionRequiresNullItem"), "action");
            }
            
            throw new ArgumentException(LocalizationManager.Instance.Get("ERR_MustBeResetAddOrRemoveActionForCtor"), "action");
        }

        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, IList changedItems)
        {
            this.newStartingIndex = -1;
            this.oldStartingIndex = -1;
            if (action == NotifyCollectionChangedAction.Add || action == NotifyCollectionChangedAction.Remove || action == NotifyCollectionChangedAction.Reset)
            {
                if (action != NotifyCollectionChangedAction.Reset)
                {
                    if (changedItems != null)
                    {
                        this.InitializeAddOrRemove(action, changedItems, -1);
                        return;
                    }
                    
                    throw new ArgumentNullException("changedItems");
                }
                
                if (changedItems == null)
                {
                    this.InitializeAdd(action, null, -1);
                    return;
                }
                
                throw new ArgumentException(LocalizationManager.Instance.Get("ERR_ResetActionRequiresNullItem"), "action");
            }
            
            throw new ArgumentException(LocalizationManager.Instance.Get("ERR_MustBeResetAddOrRemoveActionForCtor"), "action");
        }

        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, IList changedItems, int startingIndex)
        {
            this.newStartingIndex = -1;
            this.oldStartingIndex = -1;
            if (action == NotifyCollectionChangedAction.Add || action == NotifyCollectionChangedAction.Remove || action == NotifyCollectionChangedAction.Reset)
            {
                if (action != NotifyCollectionChangedAction.Reset)
                {
                    if (changedItems != null)
                    {
                        if (startingIndex >= -1)
                        {
                            this.InitializeAddOrRemove(action, changedItems, startingIndex);
                            return;
                        }
                        
                        throw new ArgumentException(LocalizationManager.Instance.Get("ERR_IndexCannotBeNegative"), "startingIndex");
                    }
                    
                    throw new ArgumentNullException("changedItems");
                }
                
                if (changedItems == null)
                {
                    if (startingIndex == -1)
                    {
                        this.InitializeAdd(action, null, -1);
                        return;
                    }
                    
                    throw new ArgumentException(LocalizationManager.Instance.Get("ERR_ResetActionRequiresIndexMinus1"), "action");
                }
                
                throw new ArgumentException(LocalizationManager.Instance.Get("ERR_ResetActionRequiresNullItem"), "action");
            }
            
            throw new ArgumentException(LocalizationManager.Instance.Get("ERR_MustBeResetAddOrRemoveActionForCtor"), "action");
        }

        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, object newItem, object oldItem)
        {
            this.newStartingIndex = -1;
            this.oldStartingIndex = -1;
            if (action == NotifyCollectionChangedAction.Replace)
            {
                object[] objArray = new object[1];
                objArray[0] = newItem;
                object[] objArray1 = new object[1];
                objArray1[0] = oldItem;
                this.InitializeMoveOrReplace(action, objArray, objArray1, -1, -1);
                return;
            }
            
            object[] objArray2 = new object[1];
            objArray2[0] = 2;
            throw new ArgumentException(string.Format(LocalizationManager.Instance.Get("ERR_WrongActionForCtor"), objArray2), "action");
        }

        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, object newItem, object oldItem, int index)
        {
            this.newStartingIndex = -1;
            this.oldStartingIndex = -1;
            if (action == NotifyCollectionChangedAction.Replace)
            {
                object[] objArray = new object[1];
                objArray[0] = newItem;
                object[] objArray1 = new object[1];
                objArray1[0] = oldItem;
                this.InitializeMoveOrReplace(action, objArray, objArray1, index, index);
                return;
            }
            
            object[] objArray2 = new object[1];
            objArray2[0] = 2;
            throw new ArgumentException(string.Format(LocalizationManager.Instance.Get("ERR_WrongActionForCtor"), objArray2), "action");
        }

        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, IList newItems, IList oldItems)
        {
            this.newStartingIndex = -1;
            this.oldStartingIndex = -1;
            if (action == NotifyCollectionChangedAction.Replace)
            {
                if (newItems != null)
                {
                    if (oldItems != null)
                    {
                        this.InitializeMoveOrReplace(action, newItems, oldItems, -1, -1);
                        return;
                    }
                    
                    throw new ArgumentNullException("oldItems");
                }
                
                throw new ArgumentNullException("newItems");
            }
            
            object[] objArray = new object[1];
            objArray[0] = 2;
            throw new ArgumentException(string.Format(LocalizationManager.Instance.Get("ERR_WrongActionForCtor"), objArray), "action");
        }

        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, IList newItems, IList oldItems, int startingIndex)
        {
            this.newStartingIndex = -1;
            this.oldStartingIndex = -1;
            if (action == NotifyCollectionChangedAction.Replace)
            {
                if (newItems != null)
                {
                    if (oldItems != null)
                    {
                        this.InitializeMoveOrReplace(action, newItems, oldItems, startingIndex, startingIndex);
                        return;
                    }
                    
                    throw new ArgumentNullException("oldItems");
                }
                
                throw new ArgumentNullException("newItems");
            }
            
            object[] objArray = new object[1];
            objArray[0] = 2;
            throw new ArgumentException(string.Format(LocalizationManager.Instance.Get("ERR_WrongActionForCtor"), objArray), "action");
        }

        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, object changedItem, int index, int oldIndex)
        {
            this.newStartingIndex = -1;
            this.oldStartingIndex = -1;
            if (action == NotifyCollectionChangedAction.Move)
            {
                if (index >= 0)
                {
                    object[] objArray = new object[1];
                    objArray[0] = changedItem;
                    object[] objArray1 = objArray;
                    this.InitializeMoveOrReplace(action, objArray1, objArray1, index, oldIndex);
                    return;
                }
                
                throw new ArgumentException(LocalizationManager.Instance.Get("ERR_IndexCannotBeNegative"), "index");
            }
            
            object[] objArray2 = new object[1];
            objArray2[0] = 3;
            throw new ArgumentException(string.Format(LocalizationManager.Instance.Get("ERR_WrongActionForCtor"), objArray2), "action");
        }

        public NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, IList changedItems, int index, int oldIndex)
        {
            this.newStartingIndex = -1;
            this.oldStartingIndex = -1;
            if (action == NotifyCollectionChangedAction.Move)
            {
                if (index >= 0)
                {
                    this.InitializeMoveOrReplace(action, changedItems, changedItems, index, oldIndex);
                    return;
                }
                
                throw new ArgumentException(LocalizationManager.Instance.Get("ERR_IndexCannotBeNegative"), "index");
            }
            
            object[] objArray = new object[1];
            objArray[0] = 3;
            throw new ArgumentException(string.Format(LocalizationManager.Instance.Get("ERR_WrongActionForCtor"), objArray), "action");
        }

        private void InitializeAdd(NotifyCollectionChangedAction action, IList newItems, int newStartingIndex)
        {
            object obj;
            this.action = action;
            NotifyCollectionChangedEventArgs notifyCollectionChangedEventArg = this;
            if (newItems == null)
            {
                obj = null;
            }
            else
            {
                obj = ArrayList.ReadOnly(newItems);
            }
            notifyCollectionChangedEventArg.newItems = (IList)obj;
            this.newStartingIndex = newStartingIndex;
        }

        private void InitializeAddOrRemove(NotifyCollectionChangedAction action, IList changedItems, int startingIndex)
        {
            if (action != NotifyCollectionChangedAction.Add)
            {
                if (action != NotifyCollectionChangedAction.Remove)
                {
                    //  Invariant.Assert(false, "Unsupported action: {0}", action.ToString());
                    return;
                }
                
                this.InitializeRemove(action, changedItems, startingIndex);
                return;
            }
            
            this.InitializeAdd(action, changedItems, startingIndex);
        }

        private void InitializeMoveOrReplace(NotifyCollectionChangedAction action, IList newItems, IList oldItems, int startingIndex, int oldStartingIndex)
        {
            this.InitializeAdd(action, newItems, startingIndex);
            this.InitializeRemove(action, oldItems, oldStartingIndex);
        }

        private void InitializeRemove(NotifyCollectionChangedAction action, IList oldItems, int oldStartingIndex)
        {
            object obj;
            this.action = action;
            NotifyCollectionChangedEventArgs notifyCollectionChangedEventArg = this;
            if (oldItems == null)
            {
                obj = null;
            }
            else
            {
                obj = ArrayList.ReadOnly(oldItems);
            }
            notifyCollectionChangedEventArg.oldItems = (IList)obj;
            this.oldStartingIndex = oldStartingIndex;
        }
    }
}