﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace System.ComponentModel
{
    public class PropertyChangingEventArgs : EventArgs
    {
        private readonly string propertyName;

        public virtual string PropertyName
        {
            get
            {
                return this.propertyName;
            }
        }

        public PropertyChangingEventArgs(string propertyName)
        {
            this.propertyName = propertyName;
        }
    }
}